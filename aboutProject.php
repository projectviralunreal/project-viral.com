<?php
    session_start();
?>
<html>
    <head>
        <title>ProjectViral | About</title>
    <?php include 'includes/header.php'; ?>
    <p class="mx-md-3 mx-sm-1 mt-5 pt-sm-1 pt-lg-3">&emsp;&emsp;As of 01/05/2021</p>
                <p>
                &emsp;&emsp;This is my first attempt at web development and AWS deployment. In this project, so far, we have used the following tools 
                <ol>
                    <li>PHP</li>
                    <li>HTML</li>
                    <li>CSS</li>
                    <li>AWS-EC2</li>
                    <li>AWS-RDS</li>
                    <li>AWS-SES</li>
                </ol>
                </p>
                <p>
                <br>
                &emsp;&emsp;So far I have added the following functionalities
                <ol>
                    <li>A responsive website</li>
                    <li>Website database interaction</li>
                    <li>User credential creation</li>
                    <li>User credentials checking </li>
                </ol>
                </p>
                <p>
                <br>
                &emsp;&emsp;Moving forward the aim is 
                <ol>
                    <li>Host the website on a AWS-R53 domain</li>
                    <li>Integrate it with a AWS-RDS to track stats of a game that will be later itegrated to aws</li>
                    <li>Setup game server for online multiplayer fuctionality</li>
                    <li>Setup friends system between accounts</li>
                </ol>
                </p>
                
    <?php include 'includes/footer.php'?>    
    </body>
</html>
