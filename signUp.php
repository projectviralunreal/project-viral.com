<?php
    session_start();

    //connect to user_login
    require 'includes/connectdb.php';

    
    $newUsername=$newEmail=$newPass=$checkPass='';
    //print_r($accounts);
    $errors=array('email'=>'', 'username'=>'', 'pass'=>'', 'checkPass'=> '');
    foreach ($_POST as $key=>$value)
    {
        $value=htmlspecialchars($value);
    }
    
    /*
        Username 
            1> should be 6 to 20 characters long
            2> can contain underscore
            3> alphanumeric case insensative 
        Password
            1> alphanumeric case insensative 
            2> atleast 1 special char (@, _, -)
            3> atleast 8-16 characters long
    */

    //check email
    if(isset($_POST['submit']))
    {
        if(empty($_POST['email']))
        {
            $errors['email']='Field is empty';
        }
        else
        {
            $newEmail=$_POST['email'];
            foreach($accounts as $key)
            {
                if($key['email']==$newEmail)
                {
                    $errors['email']='Email already registered';
                }
            }
            if(!filter_var($newEmail, FILTER_VALIDATE_EMAIL))
            {
                $errors['email']='Please enter valid email';
            }
        }
    }


    //check username
    if(isset($_POST['submit']))
    {
        if(empty($_POST['username']))
        {
            $errors['username']='Field is empty';
        }
        else
        {
            $newUsername=$_POST['username'];
            foreach($accounts as $key)
            {
                if($key['username']==$newUsername)
                {
                    $errors['username']='Username already taken';
                }
            }
            if(!preg_match('/^[\w]{6,20}$/',$newUsername))
            {
                $errors['username']='Username must be 6-20 characters long <br>alphanumeric and can contain underscore (_)';
            }
        }
    }

    //check pass
    if(isset($_POST['submit']))
    {
        if(empty($_POST['pass']))
        {
            $errors['pass']='Field is empty';
        }
        else
        {
            $newPass=$_POST['pass'];
            if(!preg_match('/^[\w@-]{8,16}$/',$newPass))
            {
                $errors['pass']='Username must be 8-16 characters long <br>alphanumeric and can contain @, _, -';
            }
        }
    }

    //validate checkPass
    if(isset($_POST['submit']))
    {
        if(empty($_POST['checkPass']))
        {
            $errors['checkPass']='Field is empty';
        }
        else
        {
            $checkPass=$_POST['checkPass'];
            if($_POST['pass']!=$checkPass)
            {
                $errors['checkPass']='This does not match the password';
            }
        }
    }

    //insert into db
    if(isset($_POST['submit']))
    {
        if(!array_filter($errors))
        {
            $newEmail=mysqli_real_escape_string($conn,$_POST['email']);
            $newUsername=mysqli_real_escape_string($conn,$_POST['username']);
            $newPass=mysqli_real_escape_string($conn,$_POST['pass']);
            $sql="INSERT INTO VSLAWS_Site.user_login(username, email, pass) VALUES ('$newUsername', '$newEmail', md5('$newPass'));";
            if(mysqli_query($conn,$sql))
            {
                header('Location: /signIn.php');
            }
            else
            {
                echo('Query error : '.mysqli_error($conn));
            }
            
        }
    }
    
    //close connection to db
    require 'includes/disconnectdb.php';
  

?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            ProjectViral | Sign Up
        </title>
        <?php include 'includes/header.php'; ?>
        <p class="mx-md-3 mx-sm-1 mt-5 pt-sm-1 pt-lg-3">

            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 form">
                    <form action="signUp.php" method="POST">
                    <div class="row">
                        <div class="col-md-2">
                            <label for="email">Email</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="email" value=<?php echo htmlspecialchars($newEmail)?>>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <div class="error"><?php echo $errors['email'] ?></div>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-md-2">
                            <label for="username">UserName</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="username" value=<?php echo htmlspecialchars($newUsername)?>>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <div class="error"><?php echo $errors['username'] ?></div>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-md-2">
                            <label for="pass">Password</label>
                        </div>
                        <div class="col-md-10">
                            <input type="password" name="pass" value=<?php echo htmlspecialchars($newPass)?>>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <div class="error"><?php echo $errors['pass'] ?></div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-2">
                            <label for="checkPass">Confirm Password</label>
                        </div>
                        <div class="col-md-10">
                            <input type="password" name="checkPass" value=<?php echo htmlspecialchars($checkPass)?>>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <div class="error"><?php echo $errors['checkPass'] ?></div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" name="submit" value="Submit" class="submitButton">Submit&emsp;<i class="fas fa-paper-plane"></i></i></button>
                        </div>
                    </div>

                        
                    </form>
                    <br>

                    </div>
                </div>
            </div>

        </p>
        

        <?php include 'includes/footer.php'; ?>
        
    </body>
</html>