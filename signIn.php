<?php
    //connect to user_login
    require 'includes/connectdb.php';

    $username=$pass=$email='';
    $errors=array('username'=>'', 'pass'=>'');

    //check username
    if(isset($_POST['submit']))
    {
        if(empty($_POST['username']))
        {
            $errors['username']='Field is empty';
        }
        else
        {
            $username=$_POST['username'];
            $errors['username']='No such username found';
            foreach($accounts as $key)
            {
                if($key['username']==$username)
                {
                    $errors['username']='';
                    $email=$key['email'];
                }
            }
        }
    }

    //check pass
    if(isset($_POST['submit']))
    {
        if(empty($_POST['pass']))
        {
            $errors['pass']='Field is empty';
        }
        else
        {
            $pass=$_POST['pass'];
            $errors['pass']='Password does not match';
            foreach($accounts as $key)
            {
                if($key['pass']==md5($pass))
                {
                    $errors['pass']='';
                }
            }
        }
    }

    //successful login
    if(isset($_POST['submit']))
    {
        if(!array_filter($errors))
        {
            session_start();
            $_SESSION['username']=$username;
            $_SESSION['email']=$email;
            header("Location: /index.php");
        }
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            ProjectViral | Sign In
        </title>
        <?php include 'includes/header.php'; ?>
        <p class="mx-md-3 mx-sm-1 mt-5 pt-sm-1 pt-lg-3">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 form">
                    <form action="signIn.php" method="POST">
                    <div class="row">
                        <div class="col-md-2">
                            <label for="username">Username</label>
                        </div>
                        <div class="col-md-10">
                            <input  type="text" name="username" value=<?php echo htmlspecialchars($username)?>>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <div class="error"><?php echo $errors['username'] ?></div>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-md-2">
                            <label for="pass">Password</label>
                        </div>
                        <div class="col-md-10">
                            <input type="password" name="pass" value=<?php echo htmlspecialchars($pass)?>>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <div class="error"><?php echo $errors['pass'] ?></div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" name="submit" value="Submit" class="submitButton">Submit&emsp;<i class="fas fa-paper-plane"></i></i></button>
                        </div>
                    </div>

                        
                    </form>
                    <br>
                    <div class="clickme">
                        <a href="/signUp.php" class="clickme">New user? Click here</a>
                    </div>

                    </div>
                </div>
            </div>

        </p>

        <?php include 'includes/footer.php'; ?>
    </body>
</html>