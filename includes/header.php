<?php


    //is the user signed in
    function isSignedIn()
    {
        if(isset($_SESSION['username']))
        {
            echo("<i class=\"fas fa-user-circle\"></i>&emsp;MY ACCOUNT");
        }
        else
        {
            echo("<i class=\"fas fa-sign-in-alt\"></i>&emsp;SIGN IN");
        }
    }

    //which page is active
    $pg = $_SERVER['REQUEST_URI'];
    $allow = array('/aboutProject.php', '/', '/signIn.php', '/contact.php','/signUp.php','/myAccount.php');
    if ( !in_array($pg, $allow) ) {
        $pg = 'default_value';
    }
    

?>  
  
  
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

  <!-- Personal CSS -->
  <link rel="stylesheet" href="/css/style.scss">

  <!-- FontAwesome -->
  <script src="https://kit.fontawesome.com/c665b5d124.js" crossorigin="anonymous"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

  <script type="text/javascript" src="js/jquery.js"></script>
  
  <script src="js/script.js"></script>

</head>
<body>

  

  <nav class="navbar navbar-expand-lg fixed-top">
    <div class="container-fluid">
      <h1 class="navbar-brand">VoidSoulLearningAWS</h1>
      <button class="navbar-toggler" type="button">
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse d-flex flex-column flex-lg-row p-2 p-lg-0 mt-5 mt-lg-0 mobileMenu" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0 align-self-stretch">
          <li class="nav-item">
            <a class="nav-link <?php if($pg == '/'){echo 'active';} ?>" aria-current="page" href="/"><i class="fas fa-home"></i>&emsp;HOME</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php if($pg == '/aboutProject.php'){echo 'active';} ?>" href="/aboutProject.php"><i class="far fa-list-alt"></i>&emsp;ABOUT</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php if($pg == '/contact.php'){echo 'active';} ?>" href="/contact.php"><i class="far fa-comment-alt"></i>&emsp;CONTACT</a>
          </li>
          
        </ul>
        <ul class="d-flex navbar-nav mb-2 mb-lg-0 align-self-stretch">
          <li class="nav-item">
            <a class="nav-link me-2 <?php if(($pg == '/signIn.php')||($pg == '/signUp.php')||($pg == '/myAccount.php')){echo 'active';} ?>" href="/redirectAcc.php"><?php isSignedIn()?></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>


        <!-- <div class="overlay"> -->
            <div class="continer-fluid">
     
