<?php

    session_start();
    if(array_filter($_SESSION))
    {
        header("Location: /myAccount.php");
    }
    else
    {
        header("Location: /signIn.php");
    }

?>