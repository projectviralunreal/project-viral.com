<?php
    session_start();
    if(isset($_POST['submit']))
    {
        
        session_destroy();
        header("Location: /index.php");
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            ProjectViral | My Account
        </title>
        <?php include 'includes/header.php'; ?>
            <div class="form">
                <p class="mx-md-3 mx-sm-1 mt-5 pt-sm-1 pt-lg-3"> 
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Username&emsp;<i class="fas fa-user-circle" style="color : #000000"></i>&emsp;:&emsp;&emsp;<?php echo $_SESSION['username'] ?><br>
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Email&emsp;&emsp;&emsp;<i class="fas fa-envelope" style="color : #000000"></i>&emsp;:&emsp;&emsp;<?php echo $_SESSION['email'] ?>
                </p>
                <p>
                    
                        <form action="/myAccount.php" method="POST">
                        <button type="submit" name="submit" value="Log Out" class="submitButton">Log Out&emsp;<i class="fas fa-sign-out-alt"></i></button>
                        </form>
                    </p>
                </div>
        <?php include 'includes/footer.php'; ?>
    </body>
</html>