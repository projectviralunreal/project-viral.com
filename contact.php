<?php
    session_start();
    //Import PHPMailer classes into the global namespace                
    //These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;
        $sub=$errorCode=$desc='';
        $email=$_SESSION['email']??'';
        $errors=array('sub'=>'','errorCode'=>'','description'=>'', 'email'=>'');
    
        //check subject
        if(isset($_POST['submit']))
        {
            if(empty($_POST['sub']))
            {
                $errors['sub']='Field is empty';
            }
            else
            {
                $sub=$_POST['sub'];
            }
        }

        //check email
        if(isset($_POST['submit']))
        {
            if(empty($_POST['email']))
            {
                $errors['email']='Field is empty';
            }
            else
            {
                $email=$_POST['email'];
                if(!filter_var($email, FILTER_VALIDATE_EMAIL))
                {
                    $errors['email']='Please enter valid email';
                }
                
            }
        }
        
    
        //check errorcode
        if(isset($_POST['submit']))
        {
            if(empty($_POST['errorCode']))
            {
                $errors['errorCode']='Field is empty';
            }
            else
            {
                $errorCode=$_POST['errorCode'];
                if((!preg_match('/^[a-z\d]{6}$/i',$errorCode))&&$errorCode!='NA')
                {
                    $errors['errorCode']='Please enter the 6 character error code or NA';
                }
            }
        }
    
        //check description
        if(isset($_POST['submit']))
        {
            if(empty($_POST['description']))
            {
                $errors['description']='Field is empty';
            }
            else
            {
                $desc=$_POST['description'];
            }
        }


        if(isset($_POST['submit']))
        {
            if(!array_filter($errors))
            {
                

                //Load Composer's autoloader
                require 'vendor/autoload.php';

                //Instantiation and passing `true` enables exceptions
                $mail = new PHPMailer(true);

                try {
                    //Server settings
                    $mail->SMTPDebug = 0;                      
                    //Enable verbose debug output
                    $mail->isSMTP();                                            
                    //Send using SMTP
                    $mail->Host       = '	
                    email-smtp.ap-south-1.amazonaws.com';                     
                    //Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   
                    //Enable SMTP authentication
                    $mail->Username   = 'AKIAW5B3V2RRUB62JHNC
                    ';                     
                    //SMTP username
                    $mail->Password   = 'BJVrU4ldOJG1bI71IxPKHaUGY2rEF5xDb+yA1UhGZ+pf
                    ';                               
                    //SMTP password
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         
                    //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                    $mail->Port       = 587;                                    
                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

                    //Recipients
                    $mail->setFrom('amentibusstercore@gmail.com', 'Mailer');
                    // $mail->addAddress('joe@example.net', 'Joe User');     
                    //Add a recipient
                    $mail->addAddress('voidsoullearningaws@gmail.com');               
                    //Name is optional
                    $mail->addReplyTo($email,'user');
                    // $mail->addCC('cc@example.com');
                    // $mail->addBCC('bcc@example.com');

                    //Attachments
                    // $mail->addAttachment('/var/tmp/file.tar.gz');         
                    //Add attachments
                    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    
                    //Optional name

                    //Content
                    $mail->isHTML(true);                                  
                    //Set email format to HTML
                    $mail->Subject = $sub;
                    $mail->Body    = 'Error Code : <b>'.$errorCode.'</b>. <br> Description :'.$desc;
                    // $mail->AltBody = ;

                    $mail->send();
                    echo '<script>alert("The email has been sent. We will move to reply as soon as possible. Thank you for your patiance.");</script>';
                } catch (Exception $e) {
                    echo "<script>alert('Sorry, failed to send the message because {$mail->ErrorInfo}');</script> ";
                }
            }
        }


   
?>


<!DOCTYPE html>
<html>
    <head>
        <title>
            ProjectViral | Contact
        </title>
        <?php include 'includes/header.php'; ?>
            
            <p class="mx-md-3 mx-sm-1 mt-5 pt-sm-1 pt-lg-3">
                <form action="contact.php" method="POST" class="contactForm" id="contact">
                    <label for="sub">Subject</label><br>
                    <input name="sub" value=<?php echo htmlspecialchars($sub)?>>
                    <div class="error"><?php echo $errors['sub'] ?></div>
                    <br>

                    <label for="email">Email</label><br>
                    <input name="email" value=<?php echo htmlspecialchars($email)?>>
                    <div class="error"><?php echo $errors['email'] ?></div>
                    <br>

                    <label for="errorCode">Error Code (enter NA if none)</label><br>
                    <input name="errorCode" value=<?php echo htmlspecialchars($errorCode)?>>
                    <div class="error"><?php echo $errors['errorCode'] ?></div>
                    <br>

                    <label for="description">Description</label><br>
                    <input name="description" value=<?php echo htmlspecialchars($desc)?>>
                    <div class="error"><?php echo $errors['description'] ?></div>
                    <br>

                    <button type="submit" name="submit" value="Submit" class="submitButton">Submit&emsp;<i class="fas fa-paper-plane"></i></button>
                </form>
            </p>
                
<?php include 'includes/footer.php'; ?>
    </body>
</html>