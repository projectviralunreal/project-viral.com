<?php
    session_start();
?>

<html>
    <head>
        <title>ProjectViral | Home</title>
    <?php include 'includes/header.php'; ?>
        <p class="mx-md-3 mx-sm-1 mt-5 pt-sm-1 pt-lg-3">&emsp;  &emsp;This is where the text goes.<br>
        &emsp;  &emsp;This is just some filler text.<br>
        &emsp;  &emsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
        labore et dolore magna aliqua. Id cursus metus aliquam eleifend mi in nulla posuere sollicitudin.
        Ut ornare lectus sit amet est placerat in egestas. Tristique risus nec feugiat in fermentum. 
        Eu ultrices vitae auctor eu augue. Mollis aliquam ut porttitor leo a diam sollicitudin tempor id.
        Et malesuada fames ac turpis egestas. Nunc aliquet bibendum enim facilisis gravida. Orci ac 
        auctor augue mauris augue neque gravida in fermentum. Nisi vitae suscipit tellus mauris a. 
        Scelerisque fermentum dui faucibus in ornare quam viverra orci. Dictumst quisque sagittis purus
        sit amet volutpat consequat. Quisque id diam vel quam elementum pulvinaetiam non quam.
        <br>
        &emsp;  &emsp;Lacus vestibulum sed arcu non odio euismod lacinia at. Id diam maecenas ultricies mi eget mauris
        . A arcu cursus vitae congue mauris rhoncus aenean vel. Ultrices neque ornare aenean euismod 
        elementum nisi. Consequat ac felis donec et odio pellentesque diam. Commodo elit at imperdiet 
        dui accumsan sit. Ac turpis egestas integer eget aliquet. Luctus accumsan tortor posuere ac ut.
        Vulputate eu scelerisque felis imperdiet. Eu nisl nunc mi ipsum faucibus vitae aliquet. 
        Consectetur adipiscing elit pellentesque habitant morbi. Mauris pharetra et ultrices neque 
        ornare. Pharetra convallis posuere morbi leo urna molestie. A erat nam at lectus urna. Id
        nibh tortor id aliquet lectus proin. Ac turpis egestas maecenas pharetra convallis posuere 
        orbi leo urna. Elementum integer enim neque volutpat ac tincidunt. Interdum velit laoreet
        id donec ultrices tincidunt arcu.
        <br>
        &emsp;  &emsp;Vestibulum lectus mauris ultrices eros. Augue mauris augue neque gravida in fermentum et. 
        Tortor vitae purus faucibus ornare. Tortor pretium viverra suspendisse potenti nullam ac tortor. 
        Sit amet porttitor eget dolor morbi non arcu. Dis parturient montes nascetur ridiculus mus mauris
        vitae ultricies leo. Viverra justo nec ultrices dui sapien eget. Lobortis feugiat vivamus at augue.
        Dolor magna eget est lorem ipsum. Eu augue ut lectus arcu bibendum at varius vel pharetra. 
        Amet dictum sit amet justo donec enim diam.
        <br>
        &emsp;  &emsp;Vestibulum lectus mauris ultrices eros. Augue mauris augue neque gravida in fermentum et. 
        Tortor vitae purus faucibus ornare. Tortor pretium viverra suspendisse potenti nullam ac tortor. 
        Sit amet porttitor eget dolor morbi non arcu. Dis parturient montes nascetur ridiculus mus mauris
        vitae ultricies leo. Viverra justo nec ultrices dui sapien eget. Lobortis feugiat vivamus at augue.
        Dolor magna eget est lorem ipsum. Eu augue ut lectus arcu bibendum at varius vel pharetra. 
        Amet dictum sit amet justo donec enim diam.
        <br>
        &emsp;  &emsp;Vestibulum lectus mauris ultrices eros. Augue mauris augue neque gravida in fermentum et. 
        Tortor vitae purus faucibus ornare. Tortor pretium viverra suspendisse potenti nullam ac tortor. 
        Sit amet porttitor eget dolor morbi non arcu. Dis parturient montes nascetur ridiculus mus mauris
        vitae ultricies leo. Viverra justo nec ultrices dui sapien eget. Lobortis feugiat vivamus at augue.
        Dolor magna eget est lorem ipsum. Eu augue ut lectus arcu bibendum at varius vel pharetra. 
        Amet dictum sit amet justo donec enim diam.
        <br>
        &emsp;  &emsp;Vestibulum lectus mauris ultrices eros. Augue mauris auguneque gravida in fermentum et. 
        ortor vitae purus faucibus ornare. Tortor pretium viverra suspendisse potenti nullam ac tortor. 
        Sit amet porttitor eget dolor morbi non arcu. Dis parturient montes nascetur ridiculus mus mauris
        vitae ultricies leo. Viverra justo nec ultrices dui sapien eget. Lobortisfeugiat vivamus at augue.
        Dolor magna eget est lorem ipsum. Eu augue ut lectus arcu bibendum at varius vel pharetra. 
        Amet dictum sit amet justo donec enim diam.
        <br>
        </p>
                
    <?php include 'includes/footer.php'?>    
    </body>
</html>